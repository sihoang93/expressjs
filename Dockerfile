FROM node:carbon

MAINTAINER Si Hoang <sihoang.io>

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install NPM
COPY package.json /usr/src/app/
RUN npm install

# Bundle app source
COPY . .

# Install
RUN npm install

EXPOSE 6789

CMD ["npm","start"]