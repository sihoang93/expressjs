const express = require('express')
const app = express()
require("dotenv").config();
const { deploy } = require('./autoDeploy')

app.get('/', (req, res) => {
    return res.send('4444')
})

app.get('/deploy', (req, res) => {
     deploy();
     return res.send('Deployed')
})

const port = process.env.PORT
app.listen(port, () => console.log(`Example app listening on port ${port}!`))